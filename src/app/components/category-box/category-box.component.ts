import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Category } from "../../models/category";

@Component({
    selector: "category-box",
    templateUrl: "category-box.component.html",
    styleUrls: ["category-box.component.css"]
})
export class CategoryBoxComponent {

    @Input() categories: Category[];
    @Output() categoryClick: EventEmitter<number> = new EventEmitter();

    categoryClicked(categoryId: number): void {
        this.categoryClick.emit(categoryId);
    }

}
