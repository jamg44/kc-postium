import { Component } from "@angular/core";
import { Router } from '@angular/router';

@Component({
    selector: "header-bar",
    templateUrl: "header-bar.component.html",
    styleUrls: ["header-bar.component.css"]
})
export class HeaderBarComponent {

    constructor(private _router: Router){ }

    search(text: string): void {
        this._router.navigate(['/search', text]);
    }
}
