import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';

import { Post } from "../../models/post";
import { User } from "../../models/user";

@Component({
    templateUrl: "post-details.component.html",
    styleUrls: ["post-details.component.css"]
})
export class PostDetailsComponent implements OnInit {

    post: Post;
    defaultUserId: number = User.defaultUser().id;

    constructor(
      private _activatedRoute: ActivatedRoute,
      private _router: Router
    ) { }

    ngOnInit(): void {
        this._activatedRoute.data.forEach((data: { post: Post}) => this.post = data.post);
        window.scrollTo(0, 0);
    }

    plainTextToHtml(text: string): string {
        return `<p>${text.replace(/\n/gi, "</p><p>")}</p>`;
    }

    showMyPosts(): void {
        this._router.navigate(['/posts/users', this.post.author.id]);
    }

    postsByCategory(category: number): void {
        this._router.navigate(['/posts/categories', category]);
    }

    updatePost(): void{
        this._router.navigate(['/post/update', this.post.id]);
    }

}
