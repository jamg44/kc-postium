import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Post } from "../../models/post";

@Component({
    selector: "post-preview",
    templateUrl: "post-preview.component.html",
    styleUrls: ["post-preview.component.css"]
})
export class PostPreviewComponent {

    @Input() post: Post;
    @Output() showDetailPost: EventEmitter<number> = new EventEmitter();
    @Output() showAuthorPosts: EventEmitter<number> = new EventEmitter();

    showMyPosts(): void {
        this.showAuthorPosts.emit(this.post.author.id);
    }

    showDetail(): void {
        this.showDetailPost.emit(this.post.id);
    }

    plainTextToHtml(text: string): string {
        return `<p>${text.replace(/\n/gi, "</p><p>")}</p>`;
    }
}
