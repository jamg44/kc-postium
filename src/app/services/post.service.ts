import { Inject, Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";

import { BackendUri } from "./settings.service";
import { Post } from "../models/post";
import { Category } from '../models/category';

@Injectable()
export class PostService {

    constructor(
        private _http: Http,
        @Inject(BackendUri) private _backendUri: any) { }

    getPosts(): Observable<Post[]> {

        const now = (new Date()).getTime();

        return this._http
                   .get(`${this._backendUri}/posts?publicationDate_lte=${now}&_sort=publicationDate&_order=DESC`)
                   .map((response: Response) => Post.fromJsonToList(response.json()));
    }

    getUserPosts(id: number): Observable<Post[]> {

        const now = (new Date()).getTime();

        return this._http
                   .get(`${this._backendUri}/posts?author.id=${id}&publicationDate_lte=${now}&_sort=publicationDate&_order=DESC`)
                   .map((response: Response) => Post.fromJsonToList(response.json()));
    }

    getCategoryPosts(id: number): Observable<Post[]> {

        const now = (new Date()).getTime();

        return this._http
            .get(`${this._backendUri}/posts?publicationDate_lte=${now}&_sort=publicationDate&_order=DESC`)
            .map((response: Response) => Post.fromJsonToList(response.json()))
            .map((posts: Post[]) =>
                // sacar los posts que...
                posts.filter((post: Post) =>
                    // alguna de sus categorias tenga el id que buscamos
                    post.categories.some((category: Category) => category.id === id)
                )
            );
    }

    getPostDetails(id: number): Observable<Post> {
        return this._http
                   .get(`${this._backendUri}/posts/${id}`)
                   .map((response: Response) => Post.fromJson(response.json()));
    }

    getPostSearch(text: string): Observable<Post[]> {
        const url = `${this._backendUri}/posts?q=${text}`;
        return this._http
            .get(url)
            .map((response: Response) => Post.fromJsonToList(response.json()));
    }

    createPost(post: Post): Observable<Post> {

        return this._http
            .post(`${this._backendUri}/posts`, post)
            .map((response: Response) => Post.fromJson(response.json()));
    }

    updatePost(post: Post): Observable<Post>{
        return this._http
            .put(`${this._backendUri}/posts/${post.id}`, post)
            .map((response: Response) => Post.fromJson(response.json()));
    }

}
